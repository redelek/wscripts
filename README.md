# Przydatne skrypty, #

Hej dzięki że odwiedziliście moje „laboratorium” ze skryptami. Piszę bo nieraz się przydają do 
Administracji serwerami Linux, będą się pojawiać też i dla windows. 
Mam dużo pracy, ale można tu zaglądać od czasu do czasu i podbierać co nie co. 
Jedno o co proszę to o zachowanie mojej oryginalnej części która znajduje się na początku skryptów.

### Opis i zadania poszczególnych plików ###

[ x ] nscripts.sh - chyba jeden z najstarszych skryptów.
niby nie wielki, a oszczędza nam czas. 
Jego zadaniem jest wpisywanie tego co zawsze powinno być na początku
każdego szanującego się skryptu czyli opis

[ x ] check_package.sh – prosty ale robi swoją robotę.
Zawsze na Ubuntu czy Debianie go uruchamiam, 
żeby sprawdził czy są moje ulubione paczki do prac administracyjnych. 
Listę paczek można zmieniać oczywiście na te które są nam potrzebne. 
Poszukajcie zmiennej **packages** i ją zmodyfikujcie na własne potrzeby

[ x ] add_user.sh - prosty skrypt dodający konto w systemie i do samby.
Skrypt trzeba uruchomić z jednym parametrem **USERNAME**, hasło jest generowane automatycznie
i zapisywane w pliku dbuser oraz wysyłane email.

[ x ] gen_file.sh - prosty skrypt do generowania plików o rozmiarze 10Mb.
 Skrypt powstał dzięki zapotrzebowaniu na przykładowe dane i pomiar czasu ich kopiowania
