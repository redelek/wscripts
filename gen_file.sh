#!/bin/bash

#################################################################
## Note: Skrypt do generowania plikow 10Mb
## Autor: piotr.redel@gmail.com REDELEK
## Date: 06-04-2021
## Usage: 
#	chmod +x gen_file.sh
#	./gen_file.sh X (gdzie X to cyfra ile plikow ma powstac) Y (pojemność 512M,1G lub 5G)
##################################################################
MAXCOUNT=$1
FILEPATH="/tmp/"
FILESIZE=$2
DT=$( date +%s )

x=0

if [ -z $MAXCOUNT ]
then
	MAXCOUNT=0
fi

if [ -z ${FILESIZE} ]
then
	FILESIZE=256M
fi

while [ $x -le $MAXCOUNT ];
do
	
        RANDOME=$((1 + $RANDOM % 100))
        FILE="tfil_${DT}_${RANDOME}"
	fallocate -l ${FILESIZE} ${FILEPATH}${FILE}
	#dd if=/dev/urandom of=${FILE} bs=${FILESIZE} count=1
	echo "Create file $FILE"
	x=$[x + 1]
done
