#!/bin/bash
##############################################################
# Skrypt napisał Piotr Redel
# kontakt: p.redel@nencki.gov.pl
# Licencja GNU
# Prosze o zachowanie i niemodyfikowanie niniejszego naglowka
# ############################################################

DT=`date +%Y-%m-%d" "%H:%M:%S`
US=$1
pass=$2
FILE="userdb"
SEND="p.redel@nencki.gov.pl"
randompw=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 9 | head -n 1)

function help {
	echo ""
	echo "---- HELP ----"
	echo -e "The script requires one argument."
	echo -e "You must enter your username * - required"
	echo -e "You can enter the password or the password will be generated automatically"
	echo -e "How to use:"
	echo -e "    ./add_user.sh USERNAME* - the password will be generated automatically"
	echo -e "    ./add_user.sh USERNAME* PASSWORD - password in unencrypted form (avoid special characters)"
	echo "" 

}

function samba_add {
    (echo "$pass"; echo "$pass") | smbpasswd -s -a $US
}

function checkroot {
if ! [ $(id -u) = 0 ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

}

checkroot

if [ -z "$1" ]; then
    help
    exit 1
    elif [ -z "$2" ]; then
	    pass=$randompw
	    US=`echo $US | tr "[:upper:]" "[:lower:]"`
	   # echo $US
	   # echo $passcode
            useradd -m -p $(echo "$pass" | openssl passwd -1 -stdin) $US && echo "User add: $US password: $pass OK" && echo "$DT User add $US $pass" >> $FILE
	    samba_add
            cat $FILE |tail -n1 |mail -s "Create user acount" $SEND
	    #echo $pass | smbpasswd -a $US -s
    else

	    US=`echo $US | tr "[:upper:]" "[:lower:]"`
            useradd -m -p $(echo "$pass" | openssl passwd -1 -stdin) $US && echo "User add: $US password: $pass OK" && echo "$DT User add $US $pass" >> $FILE
            samba_add
            cat $FILE |tail -n1 |mail -s "Create user acount" $SEND
	    #echo $pass | smbpasswd $US



fi
