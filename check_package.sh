#!/bin/bash
#------------------------------------------
# Author: Piotr Redel 'Redelek' 2018-07-07
# Contact: piotr.redel@gmail.com
# Version: ver 0.1
# MOD: 2018-07-07
# Platform: Linux Debian, Ubuntu
#------------------------------------------
# Application name : check_package
#------------------------------------------
# How to use:
#    -- download file check_package.sh
#    -- chmod +x check_package.sh
#    -- edit file vim check_package.sh
#    -- check the 'packages' table, add or remove packages that you want to install
#    -- run file command ./check_package.sh !! if you root !!
#    -- Good luck !! 
#------------------------------------------
RED='\033[0;31m'
NC='\033[0m' # No Color
GR='\033[0;32m' 
YE='\033[1;33m'

## functions
check_pkg() {
  if dpkg-query -s "${1}" 1>/dev/null 2>&1; then
    return 0   # package is installed
  else
    if apt-cache show "$1" 1>/dev/null 2>&1; then
      return 1 # package is not installed, it is available in package repository 
    else
      return 2 # package is not installed, it is not available in package repository
    fi
  fi
}

## table with packages for installation
packages=(bash nano mc vim htop tmux \
unattended-upgrades sudo most mailutils nload nmon htop multitail \
tree screen arp-scan net-tools iproute2 iputils-ping iputils-arping iputils-tracepath tcpdump acct libpam-cracklib \
ethtool ncdu speedometer iftop software-properties-common apticron swaks auditd iotop \
git)

for package_name in "${packages[@]}"
do
 if check_pkg "$package_name"
 then
  check_ver=`dpkg-query -W -f='${Version}\n' $package_name`
  echo -e "Package $package_name $check_ver ${GR}installed${NC}"
  else
   if test "$?" -eq 1
   then
    check_ver=`apt-cache show $package_name |grep Version |cut -f2 -d" "`
    echo -e "${YE}Installing${NC} the package ($package_name) from the repository - $check_ver"
    apt install $package_name -y 1>/dev/null 2>&1
   else
    echo -e "Package is not ${RED}available${NC}"
   fi
 fi
done
